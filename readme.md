# Prepare the Control Node
1. Create digitalocean droplet
2. Install terraform
3. Install ansible

# Generate SSH Key

```sh
ssh-keygen -t rsa -b 4096 -C "dokey"
chmod 600 ~/.ssh/dokey
chmod 600 ~/.ssh/dokey.pub
```

# Write the Code

# Run Terraform

```sh
terraform apply \
-var "do_token=${DO_PAT}" \
-var "pvt_key=$HOME/.ssh/dokey" \
-var "pub_key=$HOME/.ssh/dokey.pub"
```